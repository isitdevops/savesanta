#
#An Intcode program is a list of integers separated by commas (like 1,0,0,3,99).
#
#To run one, start by looking at the first integer (called position 0).
#Here, you will find an opcode - either 1, 2, or 99. The opcode indicates what to do;
#for example, 99 means that the program is finished and should immediately halt.
#Encountering an unknown opcode means something went wrong.

#Opcode 1 adds together numbers read from two positions and stores the result in a third position.
#The three integers immediately after the opcode tell you these three positions -
#the first two indicate the positions from which you should read the input values,
#and the third indicates the position at which the output should be stored.

#Opcode 2 works exactly like opcode 1, except it multiplies the two inputs instead of adding them.
#Again, the three integers after the opcode indicate where the inputs and outputs are, not their values.

#Opcode 3 takes a single integer as input and saves it to the position given by its only parameter.
#For example, the instruction 3,50 would take an input value and store it at address 50.

#Opcode 4 outputs the value of its only parameter.
#For example, the instruction 4,50 would output the value at address 50.


#Once you're done processing an opcode, move to the next one by stepping forward 4 positions.

row = [int(x) for x in open('5_1.in').read().split(',')]

for i in range(0, len(row), 4):
  print (row[i])
  operation = row[i]

  if int(row[i]) == 99:
    break
  elif int(row[i]) > 99:
    print ('<p>PARAMETER MODE Row </p>',row[i])

# 1 means add together
  elif int(row[i]) == 1:
    print ('<p>Row ',row[int(row[i+3])] ,' = ', int(row[int(row[i+1])]), ' + ', int(row[int(row[i+2])]),'</p>' )
    row[int(row[i+3])] = int(row[int(row[i+1])]) + int(row[int(row[i+2])])
#    print('loop ', i, ' after ', int(row[int(row[i+3])]))
    i += 4

# 2 means multiply
  elif int(row[i]) == 2:
    print ('<p>Row ',row[int(row[i+3])] ,' = ', int(row[int(row[i+1])]), ' * ', int(row[int(row[i+2])]) ,'</p>')
    row[int(row[i+3])] = int(row[int(row[i+1])]) * int(row[int(row[i+2])])
#    print('loop ', i, ' after ', int(row[int(row[i+3])]))
    i += 4

# 3 means save to position
  elif int(row[i]) == 3:
    print ('<p>Row ',row[int(row[i+1])] ,' = WHATEVER IS INPUT </p> ' )

# 4 means output position
  elif int(row[i]) == 4:
   print ('<p>Row ',row[int(row[i+1])], ' PRINTED </p>' )

  else:
    print ('<P>ERROR row is ',i, 'value is ', row[i],'</p>')
