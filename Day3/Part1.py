A,B,_ = open('3.in').read().split('\n')
A,B = [x.split(',') for x in [A,B]]

DX = {'L': -1, 'R': 1, 'U': 0, 'D': 0} # dictionary
DY = {'L': 0, 'R': 0, 'U': 1, 'D': -1}

def get_points(A):
# initialise variables
  x = 0
  y = 0
  ans = set()

  for cmd in A:       # is the command, e.g. D1234
    d = cmd[0]        # i.e. the CHARACTER D,U,L,R
    n = int(cmd[1:])  # i.e. the distance to move
    assert d in ['L','R','U','D'] # this is an array # IF the value is not LRUD the program will abort
    for _ in range(n):
      x+= DX[d]
      y+= DY[d]
      ans.add((x,y))
  print(ans)
  return ans

PA = get_points(A)
PB = get_points(B)
both = PA&PB
ans = min([abs(x) + abs(y) for (x,y) in both])
print (ans)
